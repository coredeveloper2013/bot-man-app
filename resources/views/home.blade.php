@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="card-header text-center">Bot Man Question Answers</h4>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="btn btn-primary" data-toggle="modal" data-target="#createQA">Add New Q/A</div>

                            <hr>

                        <div class="bot_man_table_manage">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Tags</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($questions as $qa)
                                    <tr data-tid="{{ $qa->id }}" data-question="{{ $qa->question }}" data-answer="{{ $qa->answer }}" data-tags="{{ $qa->tags }}">
                                        <td>{{ $qa->id }}</td>
                                        <td>{{ $qa->question }}</td>
                                        <td>{{ $qa->answer }}</td>
                                        <td>{{ $qa->tags }}</td>
                                        <td>{{ date('d-m-Y', strtotime($qa->created_at)) }}</td>
                                        <td><span class="fa-edit" data-toggle="modal" data-target="#editQA" id="editQAData" data-eid="{{$qa->id}}"></span> <span class="fa-delete" data-toggle="modal" data-target="#deleteQA" id="deleteQAData" data-did="{{$qa->id}}"></span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createQA" tabindex="-1" role="dialog" aria-labelledby="createQALabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/addQuestion">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Input following fields to add Q/A</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input type="text" class="form-control" name="question" id="question" placeholder="Enter question">
                            <small id="questionHelp" class="form-text text-muted">The question where would help for user.</small>
                        </div>
                        <div class="form-group">
                            <label for="answer">Answer</label>
                            <input type="text" class="form-control" name="answer" id="answer" placeholder="Enter answer">
                            <small id="answerHelp" class="form-text text-muted">Input answer for the question.</small>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input type="text" class="form-control" name="tags" id="tags" placeholder="Enter tag">
                            <small id="tagsHelp" class="form-text text-muted">Use comma for multiple tags.</small>
                        </div>
                    </div>
                    <div class="modal-footer" id="saveQA">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Question</button>
                    </div>

                </form>

            </div>
        </div>
    </div>


    <div class="modal fade" id="editQA" tabindex="-1" role="dialog" aria-labelledby="editQALabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/editQuestion">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit existing Q/A Item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" name="id" id="editID" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input type="text" class="form-control" name="question" id="questionEdit" placeholder="Enter question" value="">
                            <small id="questionHelp" class="form-text text-muted">The question where would help for user.</small>
                        </div>
                        <div class="form-group">
                            <label for="answer">Answer</label>
                            <input type="text" class="form-control" name="answer" id="answerEdit" placeholder="Enter answer" value="">
                            <small id="answerHelp" class="form-text text-muted">Input answer for the question.</small>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input type="text" class="form-control" name="tags" id="tagsEdit" placeholder="Enter tag" value="">
                            <small id="tagsHelp" class="form-text text-muted">Use comma for multiple tags.</small>
                        </div>
                    </div>
                    <div class="modal-footer" id="saveQA">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Question</button>
                    </div>

                </form>

            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteQA" tabindex="-1" role="dialog" aria-labelledby="deleteQALabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/deleteQuestion">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">You are about to delete item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center">Are you sure ?</h4>
                    </div>
                    <input type="hidden" name="id" id="deleteID" value="">
                    <div class="modal-footer" id="saveQA">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Yes</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection


