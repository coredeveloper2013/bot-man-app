<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $questions = DB::table('questions')->get();
        return view('home', ['questions'=>$questions]);
    }

    public function addQuestion(Request $request){

    	$question = $request->question;
    	$answer = $request->answer;
    	$tags = $request->tags;
	    Question::create(['question' => $question, 'answer' => $answer, 'tags' => $tags]);
	    return redirect('home');
    }

    public function editQuestion(Request $request){

    	$id = $request->id;
    	$question = $request->question;
    	$answer = $request->answer;
    	$tags = $request->tags;
	    Question::where('id', $id)->update(['question' => $question, 'answer' => $answer, 'tags' => $tags]);
	    return redirect('home');
    }

    public function deleteQuestion(Request $request){

    	$id = $request->id;
	    $the_question = Question::find($id);
	    $the_question->delete();
	    return redirect('home');
    }
}
