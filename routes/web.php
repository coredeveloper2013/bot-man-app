<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/addQuestion', 'HomeController@addQuestion')->name('addQuestion');
Route::post('/editQuestion', 'HomeController@editQuestion')->name('editQuestion');
Route::post('/deleteQuestion', 'HomeController@deleteQuestion')->name('deleteQuestion');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');


Route::get('/input/{input}', 'BotManController@handleReq');
Auth::routes();


Auth::routes();


