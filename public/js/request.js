var proxyJsonp="https://script.google.com/macros/s/AKfycbwmqG55tt2d2FcT_WQ3WjCSKmtyFpkOcdprSITn45-4UgVJnzp9/exec";
jQuery.ajaxOrig=jQuery.ajax;jQuery.ajax=function(a,b){function d(a){a=encodeURI(a).replace(/&/g,"%26");return proxyJsonp+"?url="+a+"&callback=?"}var c="object"===typeof a?a:b||{};c.url=c.url||("string"===typeof a?a:"");var c=jQuery.ajaxSetup({},c),e=function(a,c){var b=document.createElement("a");b.href=a;return c.crossOrigin&&"http"==a.substr(0,4).toLowerCase()&&"localhost"!=b.hostname&&"127.0.0.1"!=b.hostname&&b.hostname!=window.location.hostname}(c.url,c);c.proxy&&0<c.proxy.length&&(proxyJsonp=c.proxy,"object"===typeof a?
a.crossDomain=!0:"object"===typeof b&&(b.crossDomain=!0));e&&("object"===typeof a?a.url&&(a.url=d(a.url),a.charset&&(a.url+="&charset="+a.charset),a.dataType="json"):"string"===typeof a&&"object"===typeof b&&(a=d(a),b.charset&&(a+="&charset="+b.charset),b.dataType="json"));return jQuery.ajaxOrig.apply(this,arguments)};jQuery.ajax.prototype=new jQuery.ajaxOrig;jQuery.ajax.prototype.constructor=jQuery.ajax;

// base url include slash
var baseUrl = 'http://localhost:8000/';

/** loading fonts **/
var cssFileArray = ['https://fonts.googleapis.com/css?family=Open+Sans', baseUrl + 'css/bot_man_style.css'];
var cssTag;
cssFileArray.forEach(function (href) {
    cssTag = document.createElement("link");
    cssTag.href = href;
    cssTag.rel = "stylesheet";
    document.getElementsByTagName("head")[0].appendChild(cssTag);
})
// var cssTag = document.createElement("link");
// cssTag.href = "https://fonts.googleapis.com/css?family=Open+Sans";
// cssTag.rel = "stylesheet";
// document.getElementsByTagName("head")[0].appendChild(cssTag);
/** loading fonts **/

var botMan = {
    showBot: function(sel, messg) {
        var _startDiv = '<div class="chatboot" id="botManStart">';
        var _endDiv = '</div>';

        var __startBotManDiv = '<div class="__botMan__wrap" id="botManAreaWrap" style="display: none;position: fixed; bottom: 5px; right: 5px; width: 311px; border: 1px solid lightgray; border-radius: 10px 0 0 0; z-index: 999; font-family:\'Open Sans\', sans-serif;">';
        var __startBMContainer = '<div class="__botMan__container" style="width: 100%; float: left; z-index: 901;">';
        var __startBMHeadingArea = '<div class="__heading_area" style="width: 100%; float: left; height: 57px; background-color: #ffffff; border-radius: 10px 0 0 0;">\n' +
            '            <div class="__left_user_icon" style="float: left; width: 50px; padding: 5px;"><img alt="Botman Icon" src="' + baseUrl + 'images/bot_man_icn__main.png" style="width: 45px;"></div>\n' +
            '            <div class="__left_user_info" style="width: 165px; padding: 6px; float: left;">\n' +
            '                <span class="__title_lg" style="display: block; font-size: 20px; font-weight: 600;">Bot Man</span><span class="__title_sm" style="display: block; font-size: 16px;">Your assistant here</span>\n' +
            '            </div>\n' +
            '            <div class="__right__close_chat" id="hideBotManChatWin" style="cursor: pointer;float: left; width: 35px; background-color: #9a9696; height: 35px; border-radius: 50%; padding: 5px 5px; margin-top: 4px;margin-left: 21px;">\n' +
            '                <span class="__cls_icn" style="width: 100%; height: 100%; float: left; background-image: url( ' + baseUrl + 'images/bot_man__minus.png); background-position: center; background-repeat: no-repeat; background-size: 65%;"></span>\n' +
            '            </div>\n' +
            '        </div>';

        var __startBMInnerContainer = '<div class="__container_area" style="width: 100%; float: left; background-color: #f4f5f7;">\n' +
            '            <div class="__content__top_extra__info" style="padding: 3px 15px; font-size: 17px; text-align: center;">\n' +
            '                <div class="__c_extra__splash" style="top: 0; left: 0; width: 204px; height: 204px; margin: 0 auto; background: transparent url('+baseUrl+'images/bot_man_usr_img.svg) no-repeat 0 0/cover; box-shadow: 0 4px 11px -9px #4d5057!important; animation: pulse 2s infinite; border-radius: 50%;"></div>\n' +
            '            </div>\n' +
            '            <div class="__content__chat__history" id="chatContentHistory" style="width: 100%; float: left;">';

        var __innnerBMContainer = '<div class="__chat_content_item" id="chatContentItems" style="width: 100%; float: left; max-height: 280px;overflow-x: hidden;overflow-y: auto;">\n' +
            '                    <div class="__c_user__icon_area" style="width: 100%; float: left;padding: 5px">\n' +
            '                        <span class="__c__usr_icn" style="float: left; width: 40px; height: 40px; border-radius: 50%; background-image: url('+baseUrl+'images/bot_man_profile.svg); background-size: cover; background-position: center;"></span>\n' +
            '                    </div>\n' +
            '                    <div class="__c__chat_content" style="width: 70%; padding: 10px 7px; border-radius: 0 15px 15px 15px; margin: 5px; float: left; background-color: #ffffff;">\n' +
            '                        <p class="_c_txt" style="font-size: 15px;padding: 0px; margin: 0px">Hi, How May I can help you ?</p>\n' +
            '                    </div>\n' +
            '                </div>';

        var __endBMInnerContainer = '</div>\n' +
            '        <div class="__c__preloader_spin" style="display: none; width: 100px; height: 50px; float: left; background-image: url('+baseUrl+'images/bot_pre_loader.svg);background-repeat: no-repeat;margin-left: 8px;">&nbsp;</div> </div>';


        var __BMFooterContent = '<div class="__footer_area" style="width: 100%; float: left; padding: 3px; height: 40px; background-color: #f4f5f7;">\n' +
            '            <div class="__input_text__area" style="width: 78%; float: left; height: 83%; padding: 5px">\n' +
            '                <input class="_c_type_text" id="replyByBotManUser" style="width: 99%; height: 92%; border: none; font-family:\'Open Sans\', sans-serif;" type="text">\n' +
            '            </div>\n' +
            '            <div class="__sub_btn__area" style="width: 37px; float: left; height: 100%; padding-left: 5px; background-color: #9a9696; border-radius: 50%; margin-left: 8px;">\n' +
            '                <span class="__btn__submit" id="replyByBotManUserSubmit" style="cursor: pointer;width: 100%; background-image: url('+baseUrl+'images/bot_man_send_msg.png); height: 100%; float: left; background-position: center; background-repeat: no-repeat; background-size: 21px;"></span>\n' +
            '            </div>\n' +
            '        </div>';

        var __endBMContainer = '</div>';
        var __endBotManDiv = '</div>';

        var __show__BotMan_IcnBar = '<div class="__botMan__IcnBar_area" id="botManIcnEnclose" style="display: block;width: 50px;\n' +
            '    height: 50px; background-image: url('+baseUrl+'images/bot_man_profile.svg);\n' +
            '    background-size: cover; background-position: center; background-color: #fff;\n' +
            '    cursor: pointer;background-repeat: no-repeat; border-radius: 50%;\n' +
            '    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75); bottom: 31px;\n' +
            '    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75); position: absolute;\n' +
            '    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.75); right: 131px;">&nbsp;</div>';

        var __BMContainerToEmbeded = __startBotManDiv + __startBMContainer + __startBMHeadingArea +
            __startBMInnerContainer + __innnerBMContainer + __endBMInnerContainer +   __BMFooterContent +
            __endBMContainer + __endBotManDiv + __show__BotMan_IcnBar;


        jQuery('body').append(__BMContainerToEmbeded);
    }
}

jQuery(document).ready(function() {
    var pSel = '#botManStart';

    jQuery(document).on('click', '#botManIcnEnclose', function (e) {
        jQuery(this).hide(100);
        jQuery('#botManAreaWrap').show(500);
    });

    jQuery(document).on('click', '#hideBotManChatWin', function (e) {
        jQuery('#botManAreaWrap').hide(500);
        jQuery('#botManIcnEnclose').show(200);
    });

    jQuery(document).on('keyup', '#replyByBotManUser', function (e) {
        if( e.which === 13){
            var userReply = jQuery('#replyByBotManUser').val();
            displayUserReply(userReply);
        }
    })

    jQuery(document).on('click', '#replyByBotManUserSubmit', function (e) {
        var userReply = jQuery('#replyByBotManUser').val();
        displayUserReply(userReply);
    })


});

function displayUserReply(reply){
    var chatContentElement = '<div class="__c__chat_content right__align" style="width: 70%; padding: 10px 7px; border-radius: 15px 15px 0px 15px; margin: 5px; float: right; background-color: #9096a1;color:#ffffff">\n' +
        '                        <p class="_c_txt" style="font-size: 15px;padding: 0px; margin: 0px"> ' + reply + ' </p>\n' +
        '                    </div>';
    jQuery('#chatContentItems').append(chatContentElement);
    scrollToBottom();
    clearInputScreen();
    showLoadingSpinner();
    replyFromBotMan(reply);
}


function displayBotManReply(reply){
    if(reply.type === 'text'){
        var chatContentElement = '<div class="__c__chat_content" style="width: 70%; padding: 10px 7px; border-radius: 0 15px 15px 15px; margin: 5px; float: left; background-color: #ffffff;">\n' +
            '                        <p class="_c_txt" style="font-size: 15px;padding: 0px; margin: 0px"> ' + reply.text + '</p>\n' +
            '                    </div>';
    }

    setTimeout(function() {
        jQuery('#chatContentItems').append(chatContentElement);
        scrollToBottom();
        hideLoadingSpinner();
    }, 1200);

}


function replyFromBotMan(userMessage){
    jQuery.ajax({
        method: "POST",
        url: baseUrl + "botman",
        data: { driver: 'web',
            userId: +new Date(),
            message: userMessage,
            attachment: null,
            interactive: false,
            attachment_data: undefined }
    }).done(function( msg ) {
        var _mData = msg && msg.messages && msg.messages.length ? msg.messages[0] : '';
        //console.log(_mData);
        if(!!_mData){
            displayBotManReply(_mData);
        }else{
            displayBotManReply('Can not recognize you comment.');
        }
    });
}

function scrollToBottom(){
    jQuery(".__chat_content_item").animate({ scrollTop: jQuery('.__chat_content_item').prop("scrollHeight")}, 500);
}

function clearInputScreen() {
    jQuery('#replyByBotManUser').val('');
}

function showLoadingSpinner(){
    jQuery('.__c__preloader_spin').show(500);
}

function hideLoadingSpinner(){
    jQuery('.__c__preloader_spin').hide();
}